import React from 'react';
import './CountryInfo.css';

const CountryInfo = props => {
    console.log(props.countryInfo);
    return (
        <div className="Country-info">
            <div className="Country-block">
                <h2 className="Info-title">{props.countryInfo.name}</h2>

                <p className="Info-subtitle">Capital: {props.countryInfo.capital} <span></span></p>

                <p className="Info-subtitle">Population: {props.countryInfo.population} <span></span></p>

                <p className="Info-subtitle">Border with:</p>
                <ul>
                    {props.borders.map((border, index) => {
                        return (
                            <li key={index}>{border.name}</li>
                        )
                    })}
                </ul>
            </div>

            <div className="Country-flag">
                <img src={props.countryInfo.flag} width='120px' height='100px'/>
            </div>
        </div>
    );
};

export default CountryInfo;