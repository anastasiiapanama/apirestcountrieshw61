import React from 'react';
import './Country.css';

const Country = props => {
    return (
        <div className="Country" onClick={props.click}>
            <p>{props.name}</p>
        </div>
    );
};

export default Country;