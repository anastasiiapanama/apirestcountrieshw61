import './App.css';
import CountriesList from "./containers/CountriesList/CountriesList";

function App() {
  return (
    <div className="App">
      <CountriesList/>
    </div>
  );
}

export default App;
