import React, {useState ,useEffect} from 'react';
import axios from "axios";

import './CountriesList.css';
import Country from "../../components/Country/Country";
import CountryInfo from "../../components/CountryInfo/CountryInfo";

const COUNTRIES_URL = 'rest/v2/all?fields=name;alpha3Code';
const COUNTRY_URL = 'rest/v2/alpha/';

const CountriesList = () => {
    const [countries, setCountries] = useState([]);
    const [countryInfo, setCountryInfo] = useState(null);
    const [borders, setBorders] = useState([]);

    useEffect(() => {
        const fetchData = async () => {
            const countriesResponse = await axios.get(COUNTRIES_URL);
            setCountries(countriesResponse.data);
        }

        fetchData().catch(console.error);
    }, []);

    const getCountryInfo = async (code) => {
        try {
            const response = await axios.get(COUNTRY_URL + code);

            let resultBorders = [];

            for (let border of response.data.borders) {
                const result = await axios.get(COUNTRY_URL + border);
                resultBorders.push(result.data);
            }

            setCountryInfo(response.data);
            setBorders(resultBorders);
        } catch (e) {
            console.log(e);
        }
    }

    return (
        <div className={"flex"}>
            <div className="Countries-block">
                {countries.map((country, index) => (
                    <Country
                        key={index}
                        name={country.name}
                        click={() => getCountryInfo(country.alpha3Code)}
                    />
                ))}
            </div>

            <div className="Country-info">
                {countryInfo && <CountryInfo borders={borders} countryInfo={countryInfo}/>}
            </div>
        </div>
    );
};

export default CountriesList;